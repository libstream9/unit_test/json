#ifndef TEST_SRC_NAMESPACE_HPP
#define TEST_SRC_NAMESPACE_HPP

namespace std::ranges {}

namespace stream9::json {}
namespace stream9::json::literals {}

namespace stream9::ranges {}
namespace stream9::strings {}

namespace testing {

namespace json = stream9::json;

using namespace json::literals;

namespace rng { using namespace std::ranges; }
namespace rng { using namespace stream9::ranges; }

namespace str { using namespace stream9::strings; }

} // namespace testing

#endif // TEST_SRC_NAMESPACE_HPP
