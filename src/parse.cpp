#include <stream9/json/parse.hpp>

#include "namespace.hpp"
#include "data_dir.hpp"

#include <stream9/json/error.hpp>

#include <fstream>

#include <stream9/errors.hpp>
#include <stream9/filesystem/load_string.hpp>
#include <stream9/filesystem/tmpfstream.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace fs = stream9::filesystem;

BOOST_AUTO_TEST_SUITE(parse_)

    BOOST_AUTO_TEST_SUITE(from_string_)

        BOOST_AUTO_TEST_CASE(large_file_with_string_)
        {
            auto s = fs::load_string(data_dir() / "large-file.json", 30*1000*1000);

            auto v = json::parse(s);
            BOOST_TEST(v.is_array());

            auto& arr = v.get_array();
            BOOST_TEST(arr.size() == 11351);
        }

        BOOST_AUTO_TEST_CASE(parse_error_1_)
        {
            auto const s =
                "{\n"
                "   \"foo\": 100,\n"
                "   \"bar\": 20x\n"
                "}"
                ;

            json::object expected {
                { "line", 3 },
                { "column", 12 },
            };

            BOOST_CHECK_EXCEPTION(
                json::parse(s),
                stream9::error,
                [&](auto&& e) {
                    BOOST_TEST(e.why() == json::errc::syntax);
                    BOOST_TEST(e.context() == expected);
                    return true;
                });
        }

        BOOST_AUTO_TEST_CASE(parse_error_2_)
        {
            auto const s =
                "{ "
                "   \"foo\": [ \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\" ], "
                "   \"bar\": 20x "
                "} "
                ;

            json::object expected {
                { "line", 1 },
                { "column", 124 },
            };

            BOOST_CHECK_EXCEPTION(
                json::parse(s),
                stream9::error,
                [&](auto&& e) {
                    BOOST_TEST(e.why() == json::errc::syntax);
                    BOOST_TEST(e.context() == expected);
                    return true;
                });
        }

    BOOST_AUTO_TEST_SUITE_END() // from_string_

    BOOST_AUTO_TEST_SUITE(from_istream_)

        BOOST_AUTO_TEST_CASE(basic_)
        {
            std::ifstream ifs { data_dir() / "test1.json" };

            auto v = json::parse(ifs);

            auto expected = R"({ "foo": 1, "bar": 2 })"_js;

            BOOST_TEST(v == expected);
        }

        BOOST_AUTO_TEST_CASE(failed_stream_)
        {
            std::ifstream ifs { data_dir() / "xxxx.json" };

            BOOST_TEST(ifs.fail());

            auto v = json::parse(ifs);
            BOOST_TEST(v.is_null());
        }

        BOOST_AUTO_TEST_CASE(large_file_with_stream_)
        {
            std::ifstream ifs { data_dir() / "large-file.json" };

            BOOST_TEST(ifs.good());

            auto v = json::parse(ifs);
            BOOST_TEST(v.is_array());

            auto& arr = v.get_array();
            BOOST_TEST(arr.size() == 11351);
        }

        BOOST_AUTO_TEST_CASE(parse_error_1_)
        {
            fs::tmpfstream s;

            s << "{\n"
                 "   \"foo\": 100,\n"
                 "   \"bar\": 20x\n"
                 "}";

            s.seekg(0);
            BOOST_TEST_REQUIRE(s.good());

            json::object expected {
                { "pos", 29 },
            };

            BOOST_CHECK_EXCEPTION(
                json::parse(s),
                stream9::error,
                [&](auto&& e) {
                    BOOST_TEST(e.why() == json::errc::syntax);
                    BOOST_TEST(e.context() == expected);
                    return true;
                });
        }

        BOOST_AUTO_TEST_CASE(parse_error_2_)
        {
            fs::tmpfstream s;

            s << "{ "
                 "   \"foo\": [ \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\", \"long\" ], "
                 "   \"bar\": 20x "
                 "} ";

            s.seekg(0);
            BOOST_TEST_REQUIRE(s.good());

            json::object expected {
                { "pos", 124 },
            };

            BOOST_CHECK_EXCEPTION(
                json::parse(s),
                stream9::error,
                [&](auto&& e) {
                    BOOST_TEST(e.why() == json::errc::syntax);
                    BOOST_TEST(e.context() == expected);
                    return true;
                });
        }

    BOOST_AUTO_TEST_SUITE_END() // from_istream_

BOOST_AUTO_TEST_SUITE_END() // json_

} // namespace testing
