#include <stream9/json/string.hpp>

#include "namespace.hpp"

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(string_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(default_)
    {
        json::string s1;

        BOOST_TEST_REQUIRE(s1.is_string());
        BOOST_TEST(s1 == "");
    }

    BOOST_AUTO_TEST_CASE(copy_)
    {
        json::string s1 { "foo" };
        json::string s2 { s1 };

        BOOST_TEST(s2 == s1);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        json::string s1 { "foo" };
        json::string s2 { std::move(s1) };

        BOOST_TEST(s2 == "foo");
        BOOST_TEST(s1.is_null());
    }

    BOOST_AUTO_TEST_CASE(string_literal_)
    {
        json::string s1 { "foo" };

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(std_string_)
    {
        std::string s = "foo";
        json::string s1 { s };

        BOOST_TEST(s1 == s);
    }

    BOOST_AUTO_TEST_CASE(std_string_view_)
    {
        std::string_view s = "foo";
        json::string s1 { s };

        BOOST_TEST(s1 == s);
    }

    template<typename T>
    concept constructible = requires (T&& v) {
        json::string { v };
    };

    BOOST_AUTO_TEST_CASE(constructible_)
    {
        static_assert(!constructible<std::nullptr_t>);
        static_assert(constructible<json::string>);

        static_assert(!constructible<bool>);
        static_assert(!constructible<int>);
        static_assert(!constructible<double>);
        static_assert(!constructible<json::value>);
        static_assert(!constructible<json::object>);
        static_assert(!constructible<json::array>);
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(copy_)
    {
        json::string s1 = "foo";
        json::string s2;

        s2 = s1;

        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        json::string s1 = "foo";
        json::string s2;

        s2 = std::move(s1);

        BOOST_TEST(s2 == "foo");

        s1 = "bar";
        BOOST_TEST(s1 == "bar");
    }

    template<typename T>
    concept assignable = requires (T&& v, json::string s) {
        s = v;
    };

    BOOST_AUTO_TEST_CASE(assignable_)
    {
        static_assert(!assignable<std::nullptr_t>);
        static_assert(assignable<char const*>);
        static_assert(assignable<char const[]>);
        static_assert(assignable<std::string>);
        static_assert(assignable<std::string_view>);
        static_assert(assignable<json::string>);

        static_assert(!assignable<bool>);
        static_assert(!assignable<int>);
        static_assert(!assignable<double>);
        static_assert(!assignable<json::value>);
        static_assert(!assignable<json::array>);
        static_assert(!assignable<json::object>);
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_SUITE(accessor_)

    BOOST_AUTO_TEST_CASE(at_)
    {
        json::string s1 = "012345";

        BOOST_TEST(s1.at(2) == '2');

        json::string const s2 = s1;

        BOOST_TEST(s2.at(3) == '3');

        BOOST_CHECK_THROW(s2.at(9), std::out_of_range);
    }

    BOOST_AUTO_TEST_CASE(bracket_)
    {
        json::string s1 = "012345";

        BOOST_TEST(s1[2] == '2');

        json::string const s2 = s1;

        BOOST_TEST(s2[3] == '3');
    }

    BOOST_AUTO_TEST_CASE(front_const_)
    {
        json::string const s1 = "foo";

        BOOST_TEST(s1.front() == 'f');
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        json::string s1 = "foo";

        BOOST_TEST(s1.front() == 'f');
    }

    BOOST_AUTO_TEST_CASE(back_const_)
    {
        json::string const s1 = "bar";

        BOOST_TEST(s1.back() == 'r');
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        json::string s1 = "bar";

        BOOST_TEST(s1.back() == 'r');
    }

    BOOST_AUTO_TEST_CASE(data_)
    {
        json::string s1 = "foo";

        BOOST_TEST(*s1.data() == 'f');
    }

    BOOST_AUTO_TEST_CASE(data_const_)
    {
        json::string const s1 = "foo";

        BOOST_TEST(*s1.data() == 'f');
    }

    BOOST_AUTO_TEST_CASE(c_str_)
    {
        json::string const s1 = "foo";

        BOOST_TEST(*s1.c_str() == 'f');
    }

    BOOST_AUTO_TEST_CASE(begin_)
    {
        json::string s1 = "123";
        auto it = s1.begin();

        BOOST_TEST(*it == '1');
    }

    BOOST_AUTO_TEST_CASE(begin_const_)
    {
        json::string const s1 = "123";
        auto it = s1.begin();

        BOOST_TEST(*it == '1');
    }

    BOOST_AUTO_TEST_CASE(end_)
    {
        json::string s1 = "123";
        auto it = s1.end();

        BOOST_TEST(*(it-1) == '3');
    }

    BOOST_AUTO_TEST_CASE(end_const_)
    {
        json::string const s1 = "123";
        auto it = s1.end();

        BOOST_TEST(*(it-1) == '3');
    }

BOOST_AUTO_TEST_SUITE_END() // accessor_

BOOST_AUTO_TEST_SUITE(modifier_)

    BOOST_AUTO_TEST_CASE(swap_)
    {
        json::string a1 = "foo";
        json::string a2;

        a1.swap(a2);

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(swap_free_)
    {
        json::string a1 = "foo";
        json::string a2;

        swap(a1, a2);

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // modifier_

BOOST_AUTO_TEST_SUITE(comparison_)

    BOOST_AUTO_TEST_CASE(equal_with_string_)
    {
        json::string s1 = "123";
        json::string s2 = "123";

        BOOST_TEST(s1 == s2);
    }

    BOOST_AUTO_TEST_CASE(equal_with_string_literal_)
    {
        json::string s1 = "123";
        auto s2 = "123";

        BOOST_TEST(s1 == s2);
    }

    BOOST_AUTO_TEST_CASE(equal_with_std_string_)
    {
        json::string s1 = "123";
        std::string s2 = "123";

        BOOST_TEST(s1 == s2);
    }

    BOOST_AUTO_TEST_CASE(equal_with_std_string_view_)
    {
        json::string s1 = "123";
        std::string_view s2 = "123";

        BOOST_TEST(s1 == s2);
    }

    BOOST_AUTO_TEST_CASE(equal_with_value_)
    {
        json::string s1 = "123";
        json::value v1 = 1;

        BOOST_TEST(s1 != v1);
    }

    BOOST_AUTO_TEST_CASE(less_with_string_)
    {
        json::string s1 = "012";
        json::string s2 = "123";

        BOOST_TEST(s1 < s2);
    }

    BOOST_AUTO_TEST_CASE(less_with_literal_string_)
    {
        json::string s1 = "012";
        auto s2 = "123";

        BOOST_TEST(s1 < s2);
    }

    BOOST_AUTO_TEST_CASE(less_with_std_string_)
    {
        json::string s1 = "012";
        std::string s2 = "123";

        BOOST_TEST(s1 < s2);
    }

    BOOST_AUTO_TEST_CASE(less_with_string_view_)
    {
        json::string s1 = "012";
        std::string_view s2 = "123";

        BOOST_TEST(s1 < s2);
    }

    template<typename T>
    concept comparable = requires (json::string s1, T&& v) {
        s1 < v;
    };

    BOOST_AUTO_TEST_CASE(comparable_)
    {
        static_assert(comparable<json::string>);
        static_assert(!comparable<std::nullptr_t>);

        static_assert(!comparable<json::value>);
        static_assert(!comparable<bool>);
        static_assert(!comparable<int>);
        static_assert(!comparable<double>);
        static_assert(!comparable<json::array>);
        static_assert(!comparable<json::object>);
    }

BOOST_AUTO_TEST_SUITE_END() // comparison_

BOOST_AUTO_TEST_SUITE_END() // string_

BOOST_AUTO_TEST_SUITE(value_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(from_string_)
    {
        json::string s = "foo";
        json::value v1 { s };

        BOOST_TEST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(from_string_)
    {
        json::string s = "foo";
        json::value v1;

        v1 = s;

        BOOST_TEST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_CASE(emplace_string_)
{
    json::value v1;

    auto& r = v1.emplace_string();

    BOOST_TEST_REQUIRE(v1.is_string());
    BOOST_TEST(r == "");
}

BOOST_AUTO_TEST_SUITE_END() // value_

} // namespace testing
