#include <stream9/json/value.hpp>

#include "namespace.hpp"

#include <stream9/json/parse.hpp>
#include <stream9/json/value_from.hpp>

#include <string>
#include <iomanip>
#include <filesystem>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(value_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        json::value v;

        BOOST_TEST(v.is_null());
    }

    BOOST_AUTO_TEST_CASE(copy_constructor_)
    {
        json::value v0 { nullptr };
        json::value v1 { 100 };
        json::value v2 { v1 };

        BOOST_TEST(v2 == 100);
    }

    BOOST_AUTO_TEST_CASE(move_constructor_)
    {
        json::value v1 = 100;
        json::value v2 { std::move(v1) };

        BOOST_TEST(v2 == 100);
        BOOST_TEST(v1.is_null());
    }

    BOOST_AUTO_TEST_CASE(null_)
    {
        json::value v { nullptr };

        BOOST_TEST(v.is_null());
    }

    BOOST_AUTO_TEST_CASE(bool_)
    {
        json::value v { true };

        BOOST_TEST(v.is_bool());
        BOOST_TEST(v == true);
    }

    BOOST_AUTO_TEST_CASE(signed_integral_)
    {
        {
            json::value v1 { (signed char)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_int64());
        }
        {
            json::value v1 { (short)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_int64());
        }
        {
            json::value v1 { (int)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_int64());
        }
        {
            json::value v1 { (long)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_int64());
        }
        {
            json::value v1 { (long long)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_int64());
        }
    }

    BOOST_AUTO_TEST_CASE(unsigned_integral_)
    {
        {
            json::value v1 { (unsigned char)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_uint64());
        }
        {
            json::value v1 { (unsigned short)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_uint64());
        }
        {
            json::value v1 { (unsigned int)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_uint64());
        }
        {
            json::value v1 { (unsigned long)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_uint64());
        }
        {
            json::value v1 { (unsigned long long)1 };
            BOOST_TEST(v1 == 1);
            BOOST_TEST(v1.is_uint64());
        }
    }

    BOOST_AUTO_TEST_CASE(double_)
    {
        json::value v1 { (double)1 };
        BOOST_TEST(v1 == 1.0);
        BOOST_TEST(v1.is_double());
    }

    BOOST_AUTO_TEST_CASE(string_literal_)
    {
        json::value v1 { "foo" };

        BOOST_TEST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(std_string_)
    {
        std::string s = "foo";
        json::value v1 { s };

        BOOST_TEST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(std_string_view_)
    {
        std::string_view s = "foo";
        json::value v1 { s };

        BOOST_TEST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(copy_from_boost_)
    {
        boost::json::value v1 = 100;
        json::value v2 { v1 };

        BOOST_TEST(v1 == v2);
    }

    BOOST_AUTO_TEST_CASE(copy_to_boost_)
    {
        json::value v1 = 100;
        boost::json::value v2(v1.base());

        BOOST_TEST(v1 == v2);
    }

    BOOST_AUTO_TEST_CASE(move_from_boost_)
    {
        boost::json::value v1 = 100;
        json::value v2 { std::move(v1) };

        BOOST_TEST(v2 == 100);
    }

    BOOST_AUTO_TEST_CASE(move_to_boost_)
    {
        json::value v1 = 100;
        boost::json::value v2(std::move(v1.base()));

        BOOST_TEST(v2 == 100);
        BOOST_TEST(v1.is_null());
    }

    BOOST_AUTO_TEST_CASE(copy_from_boost_string_)
    {
        boost::json::string v1 = "foo";
        json::value v2 { v1 };

        BOOST_TEST(v2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(move_from_boost_string_)
    {
        boost::json::string v1 = "foo";
        json::value v2 { std::move(v1) };

        BOOST_TEST(v2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(copy_from_boost_array_)
    {
        boost::json::array a1 { 1, 2, };
        json::value v1 { a1 };

        BOOST_TEST(v1.is_array());
    }

    BOOST_AUTO_TEST_CASE(move_from_boost_array_)
    {
        boost::json::array a1 { 1, 2, };
        json::value v1 { std::move(a1) };

        BOOST_TEST(v1.is_array());
    }

    BOOST_AUTO_TEST_CASE(copy_from_boost_object_)
    {
        boost::json::object o1 { { "foo", 1 }, };
        json::value v1 { o1 };

        BOOST_TEST(v1.is_object());
    }

    BOOST_AUTO_TEST_CASE(move_from_boost_object_)
    {
        boost::json::object o1 { { "foo", 1 }, };
        json::value v1 { std::move(o1) };

        BOOST_TEST(v1.is_object());
    }

    struct foo { int value; };
    void tag_invoke(json::value_from_tag, json::value& v, foo const& f)
    {
        v = f.value;
    }

    BOOST_AUTO_TEST_CASE(from_convertible_type_)
    {
        foo f { 100 };
        json::value v1 { f };

        BOOST_REQUIRE(v1.is_int64());
        BOOST_TEST(v1.get_int64() == 100);
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(null_)
    {
        json::value v1 { 1 };
        v1 = nullptr;

        BOOST_TEST_REQUIRE(v1.is_null());
        BOOST_TEST(v1 == nullptr);
    }

    BOOST_AUTO_TEST_CASE(signed_integral_)
    {
        {
            json::value v1;
            v1 = (signed char)1;

            BOOST_TEST_REQUIRE(v1.is_int64());
            BOOST_TEST(v1 == 1);
        }
        {
            json::value v1;
            v1 = (short)1;

            BOOST_TEST_REQUIRE(v1.is_int64());
            BOOST_TEST(v1 == 1);
        }
        {
            json::value v1;
            v1 = (int)1;

            BOOST_TEST_REQUIRE(v1.is_int64());
            BOOST_TEST(v1 == 1);
        }
        {
            json::value v1;
            v1 = (long)1;

            BOOST_TEST_REQUIRE(v1.is_int64());
            BOOST_TEST(v1 == 1);
        }
        {
            json::value v1;
            v1 = (long long)1;

            BOOST_TEST_REQUIRE(v1.is_int64());
            BOOST_TEST(v1 == 1);
        }
    }

    BOOST_AUTO_TEST_CASE(unsigned_integral_)
    {
        {
            json::value v1;
            v1 = (unsigned char)1;

            BOOST_TEST_REQUIRE(v1.is_uint64());
            BOOST_TEST(v1 == 1);
        }
        {
            json::value v1;
            v1 = (unsigned short)1;

            BOOST_TEST_REQUIRE(v1.is_uint64());
            BOOST_TEST(v1 == 1);
        }
        {
            json::value v1;
            v1 = (unsigned int)1;

            BOOST_TEST_REQUIRE(v1.is_uint64());
            BOOST_TEST(v1 == 1);
        }
        {
            json::value v1;
            v1 = (unsigned long)1;

            BOOST_TEST_REQUIRE(v1.is_uint64());
            BOOST_TEST(v1 == 1);
        }
        {
            json::value v1;
            v1 = (unsigned long long)1;

            BOOST_TEST_REQUIRE(v1.is_uint64());
            BOOST_TEST(v1 == 1);
        }
    }

    BOOST_AUTO_TEST_CASE(double_)
    {
        double d = 1.0;
        json::value v1;
        v1 = d;

        BOOST_TEST_REQUIRE(v1.is_double());
        BOOST_TEST(v1 == 1.0);
    }

    BOOST_AUTO_TEST_CASE(string_literal_)
    {
        auto s = "foo";
        json::value v1;
        v1 = s;

        BOOST_TEST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(std_string_)
    {
        std::string s = "foo";
        json::value v1;
        v1 = s;

        BOOST_TEST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(std_string_view_)
    {
        std::string_view s = "foo";
        json::value v1;
        v1 = s;

        BOOST_TEST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(copy_from_boost_value_)
    {
        json::value v1;
        boost::json::value v2 = "100";

        v1 = v2;

        BOOST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "100");
        BOOST_TEST(v2 == v1);
    }

    BOOST_AUTO_TEST_CASE(move_from_boost_value_)
    {
        json::value v1;
        boost::json::value v2 = "100";

        v1 = std::move(v2);

        BOOST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "100");
        BOOST_TEST(v2 != v1);
    }

    BOOST_AUTO_TEST_CASE(copy_from_boost_string_)
    {
        json::value v1;
        boost::json::string v2 = "100";

        v1 = v2;

        BOOST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "100");
        BOOST_TEST(v2 == v1);
    }

    BOOST_AUTO_TEST_CASE(move_from_boost_string_)
    {
        json::value v1;
        boost::json::string v2 = "100";

        v1 = std::move(v2);

        BOOST_REQUIRE(v1.is_string());
        BOOST_TEST(v1 == "100");
        BOOST_TEST(v2 != v1);
    }

    BOOST_AUTO_TEST_CASE(copy_from_boost_array_)
    {
        json::value v1;
        boost::json::array const v2 { 1, 2, 3 };

        v1 = v2;

        BOOST_REQUIRE(v1.is_array());
        BOOST_TEST(v1 == v2);
    }

    BOOST_AUTO_TEST_CASE(move_from_boost_array_)
    {
        json::value v1;
        boost::json::array v2 { 1, 2, 3 };

        v1 = std::move(v2);

        BOOST_REQUIRE(v1.is_array());
        BOOST_TEST(v1 == (json::array { 1, 2, 3 }));
        BOOST_TEST(v2 != v1);
    }

    BOOST_AUTO_TEST_CASE(copy_from_boost_object_)
    {
        json::value v1;
        boost::json::object const v2 { { "x", 1 } };

        v1 = v2;

        BOOST_REQUIRE(v1.is_object());
        BOOST_TEST(v1 == v2);
    }

    BOOST_AUTO_TEST_CASE(move_from_boost_object_)
    {
        json::value v1;
        boost::json::object v2 { { "x", 1 } };

        v1 = std::move(v2);

        BOOST_REQUIRE(v1.is_object());
        BOOST_TEST(v1 == (json::object { { "x", 1 } }));
        BOOST_TEST(v2 != v1);
    }

    struct foo { int value; };
    void tag_invoke(json::value_from_tag, json::value& v, foo const& f)
    {
        v = f.value;
    }

    BOOST_AUTO_TEST_CASE(from_convertible_type_)
    {
        foo f { 100 };
        json::value v1;
        v1 = f;

        BOOST_REQUIRE(v1.is_int64());
        BOOST_TEST(v1.get_int64() == 100);
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_CASE(kind_)
{
    json::value v;

    BOOST_TEST(v.kind() == json::kind::null);
}

BOOST_AUTO_TEST_CASE(get_allocator_)
{
    json::value v;

    auto alloc = v.get_allocator();

    using Alloc = std::pmr::polymorphic_allocator<boost::json::standalone::value>;

    static_assert(std::same_as<decltype(alloc), Alloc>);
}

BOOST_AUTO_TEST_CASE(emplace_null_)
{
    json::value v1 = 1;

    v1.emplace_null();

    BOOST_TEST(v1.is_null());
}

BOOST_AUTO_TEST_CASE(emplace_bool_)
{
    json::value v1;

    bool& b = v1.emplace_bool();

    BOOST_TEST_REQUIRE(v1.is_bool());
    BOOST_TEST(b == false);
}

BOOST_AUTO_TEST_CASE(emplace_int64_)
{
    json::value v1;

    int64_t& r = v1.emplace_int64();

    BOOST_TEST_REQUIRE(v1.is_int64());
    BOOST_TEST(r == 0);
}

BOOST_AUTO_TEST_CASE(emplace_uint64_)
{
    json::value v1;

    uint64_t& r = v1.emplace_uint64();

    BOOST_TEST_REQUIRE(v1.is_uint64());
    BOOST_TEST(r == 0);
}

BOOST_AUTO_TEST_CASE(emplace_double_)
{
    json::value v1;

    double& r = v1.emplace_double();

    BOOST_TEST_REQUIRE(v1.is_double());
    BOOST_TEST(r == 0.0);
}

BOOST_AUTO_TEST_CASE(swap_1_)
{
    json::value v1 = 1;
    json::value v2;

    v1.swap(v2);

    BOOST_TEST(v1 == nullptr);
    BOOST_TEST(v2 == 1);
}

BOOST_AUTO_TEST_CASE(swap_2_)
{
    json::value v1 = 1;
    json::value v2;

    swap(v1, v2);

    BOOST_TEST(v1 == nullptr);
    BOOST_TEST(v2 == 1);
}

BOOST_AUTO_TEST_CASE(swap_with_boost_)
{
    boost::json::value v1 = 1;
    json::value v2;

    v1.swap(v2);

    BOOST_TEST(v1 == nullptr);
    BOOST_TEST(v2 == 1);
}

BOOST_AUTO_TEST_CASE(to_number_1_)
{
    json::value v1 = 1;
    std::error_code ec;

    BOOST_TEST(v1.to_number<int>(ec) == 1);
    BOOST_TEST(!ec);
}

BOOST_AUTO_TEST_CASE(to_number_2_)
{
    json::value v1 = 1;

    BOOST_TEST(v1.to_number<int>() == 1);
}

BOOST_AUTO_TEST_SUITE(comparison_)

    BOOST_AUTO_TEST_CASE(with_boost_)
    {
        json::value v1 = 100;
        boost::json::value v2 = 100;

        BOOST_TEST(v1 == v2);
    }

BOOST_AUTO_TEST_SUITE_END() // comparison_

BOOST_AUTO_TEST_CASE(serialize_)
{
    json::object obj {
        { "key1", 1 },
        { "key2", "value" },
        { "key3", { 2.0, 1.0, 0.0, false } },
    };

    std::ostringstream oss;
    obj.serialize(oss, "  ");

    auto expected =
        "{\n"
        "  \"key1\": 1,\n"
        "  \"key2\": \"value\",\n"
        "  \"key3\": [\n"
        "    2E0,\n"
        "    1E0,\n"
        "    0E0,\n"
        "    false\n"
        "  ]\n"
        "}";

    BOOST_TEST(oss.str() == expected);

    auto v = json::parse(oss.str());
    BOOST_TEST(v == obj);
}

BOOST_AUTO_TEST_CASE(to_string_)
{
    json::object obj {
        { "key1", 1 },
        { "key2", "value" },
        { "key3", {
                { "child1", 5 },
                { "child2", "foo" },
            }
        }
    };

    auto s = obj.to_string("  ");

    auto expected =
        "{\n"
        "  \"key1\": 1,\n"
        "  \"key2\": \"value\",\n"
        "  \"key3\": {\n"
        "    \"child1\": 5,\n"
        "    \"child2\": \"foo\"\n"
        "  }\n"
        "}";

    BOOST_TEST(s == expected);

    auto v = json::parse(s);
    BOOST_TEST(v == obj);
}

BOOST_AUTO_TEST_CASE(iomanip_)
{
    json::object obj {
        { "key1", 1 },
        { "key2", "value" },
        { "key3", { 2.0, 1.0, 0.0, false } },
        { "empty_obj", json::object() },
        { "empty_arr", json::array() },
    };

    std::ostringstream oss;
    oss << std::setw(4) << obj;

    auto expected =
        "{\n"
        "    \"key1\": 1,\n"
        "    \"key2\": \"value\",\n"
        "    \"key3\": [\n"
        "        2E0,\n"
        "        1E0,\n"
        "        0E0,\n"
        "        false\n"
        "    ],\n"
        "    \"empty_obj\": {},\n"
        "    \"empty_arr\": []\n"
        "}";

    BOOST_TEST(oss.str() == expected);

    auto v = json::parse(oss.str());
    BOOST_TEST(v == obj);
}

BOOST_AUTO_TEST_CASE(initializer_list_bug_fix_)
{
    json::value v { 1 };
    json::object o1;
    json::array a1;

    json::object obj {
        { "v1", v },
        { "o1", o1 },
        { "a1", a1 },
    };

    auto expected = R"({"v1":1,"o1":{},"a1":[]})";
    BOOST_TEST(obj.to_string() == expected);
}

struct foo {};

void
tag_invoke(json::value_from_tag, json::value& jv, foo const&)
{
    jv = true;
}

BOOST_AUTO_TEST_CASE(initializer_list_bug_fix_2_)
{
    foo f;

    json::object obj {
        { "f1", f },
        { "f2", foo() },
    };

    auto expected = R"({"f1":true,"f2":true})";
    BOOST_TEST(obj.to_string() == expected);
}

BOOST_AUTO_TEST_CASE(value_from_tag_)
{
    static_assert(std::default_initializable<json::value_from_tag>);
    static_assert(std::constructible_from<json::value_from_tag,
                                          boost::json::value_from_tag >);
}

BOOST_AUTO_TEST_CASE(path_)
{
    namespace fs = std::filesystem;
    fs::path p { "/foo" };

    BOOST_TEST(json::value_from(p) == "/foo");
}

BOOST_AUTO_TEST_SUITE_END() // value_

} // namespace testing
