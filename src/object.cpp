#include <stream9/json/object.hpp>

#include "namespace.hpp"

#include <map>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(object_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(default_)
    {
        json::object o1;

        BOOST_TEST_REQUIRE(o1.is_object());
        BOOST_TEST(o1.empty());
    }

    BOOST_AUTO_TEST_CASE(copy_)
    {
        json::object o1 { { "foo", 1 } };
        json::object o2 { o1 };

        BOOST_TEST_REQUIRE(o2.is_object());
        BOOST_TEST(o2.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        json::object o1 { { "foo", 1 } };
        json::object o2 { std::move(o1) };

        BOOST_TEST_REQUIRE(o2.is_object());
        BOOST_TEST(o2.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(with_count_)
    {
        json::object o1(10);

        BOOST_TEST(o1.capacity() == 10);
    }

    BOOST_AUTO_TEST_CASE(with_iterator_)
    {
        std::map<std::string, int> m1 {
            { "foo", 1 },
            { "bar", 2 },
        };

        json::object o1(m1.begin(), m1.end(), 100);

        BOOST_TEST(o1.size() == 2);
        BOOST_TEST(o1.capacity() == 100);
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(copy_)
    {
        json::object o1 { { "foo", 1 } };
        json::object o2;

        o2 = o1;

        BOOST_TEST_REQUIRE(o2.is_object());
        BOOST_TEST(o2.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        json::object o1 { { "foo", 1 } };
        json::object o2;

        o2 = std::move(o1);

        BOOST_TEST_REQUIRE(o2.is_object());
        BOOST_TEST(o2.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(with_initializer_list_)
    {
        json::object o1;

        o1 = {
            { "foo", 1 },
        };

        BOOST_TEST_REQUIRE(o1.is_object());
        BOOST_TEST(o1.size() == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_SUITE(accessor_)

    BOOST_AUTO_TEST_CASE(at_)
    {
        json::object o1 {
            { "foo", 1 },
        };

        auto& rv = o1.at("foo");

        BOOST_TEST(rv == 1);
    }

    BOOST_AUTO_TEST_CASE(at_const_)
    {
        json::object const o1 {
            { "foo", 1 },
        };

        auto& rv = o1.at("foo");

        BOOST_TEST(rv == 1);
    }

    BOOST_AUTO_TEST_CASE(bracket_)
    {
        json::object o1 {
            { "foo", 1 },
        };

        auto& rv = o1["foo"];

        BOOST_TEST(rv == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // accessor_

BOOST_AUTO_TEST_SUITE(modifier_)

    BOOST_AUTO_TEST_CASE(insert_key_value_pair_)
    {
        json::object o1;

        o1.insert(std::make_pair("foo", 1));

        BOOST_TEST(o1.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(insert_with_iterator_)
    {
        json::object o1;

        std::map<std::string, int> m1 {
            { "foo", 1 },
            { "bar", 2 },
        };

        o1.insert(m1.begin(), m1.end());

        BOOST_TEST(o1.size() == 2);
    }

    BOOST_AUTO_TEST_CASE(insert_with_initializer_list_)
    {
        json::object o1;

        o1.insert({
            { "foo", 1 },
            { "bar", 2 },
        });

        BOOST_TEST(o1.size() == 2);
    }

    BOOST_AUTO_TEST_CASE(insert_or_assign_)
    {
        json::object o1;

        auto [it1, ok1] = o1.insert_or_assign("foo", 1);

        BOOST_TEST(ok1);
        BOOST_TEST((*it1).second == 1);

        auto [it2, ok2] = o1.insert_or_assign("foo", 2);

        BOOST_TEST(!ok2);
        BOOST_TEST((*it2).second == 2);
    }

    BOOST_AUTO_TEST_CASE(emplace_)
    {
        json::object o1;

        auto [it, ok] = o1.emplace("foo", 1);

        BOOST_TEST(ok);
        BOOST_TEST((*it).first == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // modifier_

BOOST_AUTO_TEST_SUITE_END() // object_

BOOST_AUTO_TEST_SUITE(value_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(from_object_)
    {
        json::object o1 { { "foo", 1 } };
        json::value v1 { o1 };

        BOOST_TEST(v1.is_object());
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(from_object_)
    {
        json::object o1 { { "foo", 1 } };
        json::value v1;

        v1 = o1;

        BOOST_TEST(v1.is_object());
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_CASE(emplace_object_)
{
    json::value v1;

    v1.emplace_object();

    BOOST_TEST(v1.is_object());
}

BOOST_AUTO_TEST_SUITE_END() // value_

} // namespace testing
