#include <stream9/json/format.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(format_)

    BOOST_AUTO_TEST_CASE(array_)
    {
        auto s1 = json::format({ 1, 2, 3 });

        auto expected = R"([1,2,3])";
        BOOST_TEST(s1 == expected);
    }

    BOOST_AUTO_TEST_CASE(object_)
    {
        auto s1 = json::format({
            { "foo", 1 },
            { "bar", "xyzzy" },
        });

        auto expected = R"({"foo":1,"bar":"xyzzy"})";
        BOOST_TEST(s1 == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // format_

} // namespace testing
