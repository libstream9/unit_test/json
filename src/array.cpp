#include <stream9/json/array.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(array_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(default_)
    {
        json::array a1;

        BOOST_TEST_REQUIRE(a1.is_array());
        BOOST_TEST(a1.empty());
    }

    BOOST_AUTO_TEST_CASE(copy_)
    {
        json::array a1;
        a1.push_back(1);

        json::array a2(a1);

        BOOST_TEST(a2.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        json::array a1;
        a1.push_back(1);

        json::array a2(std::move(a1));

        BOOST_TEST(a2.size() == 1);
        BOOST_TEST(a1.is_null());
    }

    BOOST_AUTO_TEST_CASE(with_count_)
    {
        json::array a1(5);

        BOOST_TEST(a1.size() == 5);
    }

    BOOST_AUTO_TEST_CASE(with_count_and_value_)
    {
        json::array a1(5, 1);

        BOOST_TEST(a1.size() == 5);
    }

    BOOST_AUTO_TEST_CASE(with_iterator_)
    {
        std::vector<int> v { 1, 2, 3 };
        json::array a1(v.begin(), v.end());

        BOOST_TEST(a1.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(with_initializer_list_)
    {
        json::array a1 { 1, 2, 3 };

        BOOST_TEST(a1.size() == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(copy_)
    {
        json::array a1 { 1 };

        json::array a2;
        a2 = a1;

        BOOST_TEST(a1.size() == 1);
        BOOST_TEST(a2.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        json::array a1 { 1 };

        json::array a2;
        a2 = std::move(a1);

        BOOST_TEST(a2.size() == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_SUITE(iterator_)

    BOOST_AUTO_TEST_CASE(begin_)
    {
        json::array a1 { 1, 2, 3 };

        auto it = a1.begin();

        BOOST_TEST(*it == 1);
    }

    BOOST_AUTO_TEST_CASE(begin_const_)
    {
        json::array const a1 { 1, 2, 3 };

        auto it = a1.begin();

        BOOST_TEST(*it == 1);
    }

    BOOST_AUTO_TEST_CASE(end_)
    {
        json::array a1 { 1, 2, 3 };

        auto it = a1.end();

        BOOST_TEST(*(it-1) == 3);
    }

    BOOST_AUTO_TEST_CASE(end_const_)
    {
        json::array const a1 { 1, 2, 3 };

        auto it = a1.end();

        BOOST_TEST(*(it-1) == 3);
    }

    BOOST_AUTO_TEST_CASE(cbegin_)
    {
        json::array const a1 { 1, 2, 3 };

        auto it = a1.cbegin();

        BOOST_TEST(*it == 1);
    }

    BOOST_AUTO_TEST_CASE(cend_)
    {
        json::array const a1 { 1, 2, 3 };

        auto it = a1.cend();

        BOOST_TEST(*(it-1) == 3);
    }

    BOOST_AUTO_TEST_CASE(rbegin_)
    {
        json::array a1 { 1, 2, 3 };

        auto it = a1.rbegin();

        BOOST_TEST(*it == 3);
    }

    BOOST_AUTO_TEST_CASE(rbegin_const_)
    {
        json::array const a1 { 1, 2, 3 };

        auto it = a1.rbegin();

        BOOST_TEST(*it == 3);
    }

    BOOST_AUTO_TEST_CASE(rend_)
    {
        json::array a1 { 1, 2, 3 };

        auto it = a1.rend();

        BOOST_TEST(*(it-1) == 1);
    }

    BOOST_AUTO_TEST_CASE(rend_const_)
    {
        json::array const a1 { 1, 2, 3 };

        auto it = a1.rend();

        BOOST_TEST(*(it-1) == 1);
    }

    BOOST_AUTO_TEST_CASE(crbegin_)
    {
        json::array const a1 { 1, 2, 3 };

        auto it = a1.crbegin();

        BOOST_TEST(*it == 3);
    }

    BOOST_AUTO_TEST_CASE(crend_)
    {
        json::array const a1 { 1, 2, 3 };

        auto it = a1.crend();

        BOOST_TEST(*(it-1) == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // iterator_

BOOST_AUTO_TEST_SUITE(modifier_)

    BOOST_AUTO_TEST_CASE(insert_with_iterator_)
    {
        std::vector<int> v { 1, 2, 3 };
        json::array a;

        a.insert(a.end(), v.begin(), v.end());

        BOOST_TEST(a.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(emplace_with_iterator_)
    {
        json::array a;

        a.emplace(a.end(), 1);

        BOOST_TEST(a.size() == 1);
    }

    BOOST_AUTO_TEST_CASE(swap_)
    {
        json::array a1 { 1, 2, 3 };
        json::array a2;

        a1.swap(a2);

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(swap_free_)
    {
        json::array a1 { 1, 2, 3 };
        json::array a2;

        swap(a1, a2);

        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == 3);
    }

BOOST_AUTO_TEST_SUITE_END() // modifier_

BOOST_AUTO_TEST_SUITE(comparison_)

    BOOST_AUTO_TEST_CASE(equal_)
    {
        json::array a1 { 1, 2, 3 };
        json::array a2 { 1, 2, 3 };

        BOOST_TEST(a1 == a2);
    }

    BOOST_AUTO_TEST_CASE(equal_with_value_)
    {
        json::array a1 { 1, 2, 3 };
        json::value v1;

        BOOST_TEST(a1 != v1);
    }

BOOST_AUTO_TEST_SUITE_END() // comparison_

BOOST_AUTO_TEST_SUITE_END() // array_

BOOST_AUTO_TEST_SUITE(value_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(from_array_)
    {
        json::array a1 { 1, 2, 3 };
        json::value v1 { a1 };

        BOOST_TEST(v1.is_array());
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(from_array_)
    {
        json::array a1 { 1, 2, 3 };
        json::value v1;

        v1 = a1;

        BOOST_TEST(v1.is_array());
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_CASE(emplace_array_)
{
    json::value v1;

    v1.emplace_array();

    BOOST_TEST(v1.is_array());
}

BOOST_AUTO_TEST_SUITE_END() // value_

} // namespace testing
