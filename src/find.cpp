#include <stream9/json/find.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(find_value_)

    BOOST_AUTO_TEST_SUITE(level_1_)

        BOOST_AUTO_TEST_CASE(object_success_)
        {
            json::object obj { { "foo", true }, };

            auto o_v = json::find_value(obj, "foo");

            BOOST_REQUIRE(o_v);
            BOOST_CHECK(o_v->is_bool());
            BOOST_CHECK(o_v->get_bool() == true);
        }

        BOOST_AUTO_TEST_CASE(object_const_success_)
        {
            json::object const obj { { "foo", true }, };

            auto o_v = json::find_value(obj, "foo");

            BOOST_REQUIRE(o_v);
            BOOST_CHECK(o_v->is_bool());
            BOOST_CHECK(o_v->get_bool() == true);
        }

        BOOST_AUTO_TEST_CASE(object_failure_1_)
        {
            json::value v = "bar";

            auto o_v = json::find_value(v, "bar");

            BOOST_REQUIRE(!o_v);
        }

        BOOST_AUTO_TEST_CASE(object_failure_2_)
        {
            json::object obj { { "foo", true }, };

            auto o_v = json::find_value(obj, "bar");

            BOOST_REQUIRE(!o_v);
        }

        BOOST_AUTO_TEST_CASE(array_success_)
        {
            json::array arr { 1, 2, 3 };

            auto o_v = json::find_value(arr, 1);

            BOOST_REQUIRE(o_v);
            BOOST_CHECK(o_v->is_int64());
            BOOST_CHECK(o_v->get_int64() == 2);
        }

        BOOST_AUTO_TEST_CASE(array_const_success_)
        {
            json::array const arr { 1, 2, 3 };

            auto o_v = json::find_value(arr, 1);

            BOOST_REQUIRE(o_v);
            BOOST_CHECK(o_v->is_int64());
            BOOST_CHECK(o_v->get_int64() == 2);
        }

        BOOST_AUTO_TEST_CASE(array_failure_1_)
        {
            json::value v = "foo";

            auto o_v = json::find_value(v, 5);

            BOOST_REQUIRE(!o_v);
        }

        BOOST_AUTO_TEST_CASE(array_failure_2_)
        {
            json::array arr { 1, 2, 3 };

            auto o_v = json::find_value(arr, 5);

            BOOST_REQUIRE(!o_v);
        }

    BOOST_AUTO_TEST_SUITE_END() // level_1_

    BOOST_AUTO_TEST_SUITE(level_2_)

        BOOST_AUTO_TEST_SUITE(object_object_)

            BOOST_AUTO_TEST_CASE(success_)
            {
                json::object v {
                    { "foo", { { "bar", true }, } },
                };

                auto o_v = json::find_value(v, "foo", "bar");

                BOOST_REQUIRE(o_v);
                BOOST_CHECK(o_v->is_bool());
                BOOST_CHECK(o_v->get_bool() == true);
            }

            BOOST_AUTO_TEST_CASE(success_const_)
            {
                json::object const v {
                    { "foo", { { "bar", true }, } },
                };

                auto o_v = json::find_value(v, "foo", "bar");

                BOOST_REQUIRE(o_v);
                BOOST_CHECK(o_v->is_bool());
                BOOST_CHECK(o_v->get_bool() == true);
            }

            BOOST_AUTO_TEST_CASE(failure_1_)
            {
                json::array v { 1, { { "foo", true } } };

                auto o_v = json::find_value(v, "foo", "foo");

                BOOST_REQUIRE(!o_v);
            }

            BOOST_AUTO_TEST_CASE(failure_2_)
            {
                json::object v {
                    { "bar", { { "foo", true }, } },
                };

                auto o_v = json::find_value(v, "foo", "foo");

                BOOST_REQUIRE(!o_v);
            }

        BOOST_AUTO_TEST_SUITE_END() // object_object_

        BOOST_AUTO_TEST_SUITE(object_array_)

            BOOST_AUTO_TEST_CASE(success_)
            {
                json::object v {
                    { "foo", { 1, true } },
                };

                auto o_v = json::find_value(v, "foo", 1);

                BOOST_REQUIRE(o_v);
                BOOST_CHECK(o_v->is_bool());
                BOOST_CHECK(o_v->get_bool() == true);
            }

            BOOST_AUTO_TEST_CASE(success_const_)
            {
                json::object const v {
                    { "foo", { 1, true } },
                };

                auto o_v = json::find_value(v, "foo", 1);

                BOOST_REQUIRE(o_v);
                BOOST_CHECK(o_v->is_bool());
                BOOST_CHECK(o_v->get_bool() == true);
            }

            BOOST_AUTO_TEST_CASE(failure_1_)
            {
                json::array v { 1, 2, };

                auto o_v = json::find_value(v, "foo", 1);

                BOOST_REQUIRE(!o_v);
            }

            BOOST_AUTO_TEST_CASE(failure_2_)
            {
                json::object v {
                    { "bar", { 1, true } },
                };

                auto o_v = json::find_value(v, "foo", 1);

                BOOST_REQUIRE(!o_v);
            }

        BOOST_AUTO_TEST_SUITE_END() // object_array_

        BOOST_AUTO_TEST_SUITE(array_object_)

            BOOST_AUTO_TEST_CASE(success_)
            {
                json::array v {
                    1, { { "foo", true } },
                };

                auto o_v = json::find_value(v, 1, "foo");

                BOOST_REQUIRE(o_v);
                BOOST_CHECK(o_v->is_bool());
                BOOST_CHECK(o_v->get_bool() == true);
            }

            BOOST_AUTO_TEST_CASE(success_const_)
            {
                json::array const v {
                    1, { { "foo", true } },
                };

                auto o_v = json::find_value(v, 1, "foo");

                BOOST_REQUIRE(o_v);
                BOOST_CHECK(o_v->is_bool());
                BOOST_CHECK(o_v->get_bool() == true);
            }

            BOOST_AUTO_TEST_CASE(failure_1_)
            {
                json::value v { "foo" };

                auto o_v = json::find_value(v, 1, "foo");

                BOOST_REQUIRE(!o_v);
            }

            BOOST_AUTO_TEST_CASE(failure_2_)
            {
                json::array v { 1 };

                auto o_v = json::find_value(v, 1, "foo");

                BOOST_REQUIRE(!o_v);
            }

        BOOST_AUTO_TEST_SUITE_END() // array_object_

        BOOST_AUTO_TEST_SUITE(array_array_)

            BOOST_AUTO_TEST_CASE(success_)
            {
                json::array v { 1, { 1, 2 } };

                auto o_v = json::find_value(v, 1, 1);

                BOOST_REQUIRE(o_v);
                BOOST_CHECK(o_v->is_int64());
                BOOST_CHECK(o_v->get_int64() == 2);
            }

            BOOST_AUTO_TEST_CASE(success_const_)
            {
                json::array const v { 1, { 1, 2 } };

                auto o_v = json::find_value(v, 1, 1);

                BOOST_REQUIRE(o_v);
                BOOST_CHECK(o_v->is_int64());
                BOOST_CHECK(o_v->get_int64() == 2);
            }

            BOOST_AUTO_TEST_CASE(failure_1_)
            {
                json::array v { 0, 1 };

                auto o_v = json::find_value(v, 1, 1);

                BOOST_REQUIRE(!o_v);
            }

            BOOST_AUTO_TEST_CASE(failure_2_)
            {
                json::array v { 1, { 2 } };

                auto o_v = json::find_value(v, 1, 1);

                BOOST_REQUIRE(!o_v);
            }

        BOOST_AUTO_TEST_SUITE_END() // array_array_

    BOOST_AUTO_TEST_SUITE_END() // level_2_

BOOST_AUTO_TEST_SUITE_END() // find_value_

BOOST_AUTO_TEST_SUITE(find_object_)

    BOOST_AUTO_TEST_SUITE(level_0_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            json::object v { { "x", true } };

            auto o_v = json::find_object(v);

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->contains("x"));
        }

        BOOST_AUTO_TEST_CASE(success_const_)
        {
            json::object const v { { "x", true } };

            auto o_v = json::find_object(v);

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->contains("x"));
        }

        BOOST_AUTO_TEST_CASE(failure_)
        {
            json::value v { "foo" };

            auto o_v = json::find_object(v);

            BOOST_REQUIRE(!o_v);
        }

    BOOST_AUTO_TEST_SUITE_END() // level_0_

    BOOST_AUTO_TEST_SUITE(level_1_)

        BOOST_AUTO_TEST_SUITE(object_)

            BOOST_AUTO_TEST_CASE(success_)
            {
                json::object v { { "x", { { "y", true } } } };

                auto o_v = json::find_object(v, "x");

                BOOST_REQUIRE(o_v);
                BOOST_TEST(o_v->contains("y"));
            }

            BOOST_AUTO_TEST_CASE(success_const_)
            {
                json::object v { { "x", { { "y", true } } } };

                auto o_v = json::find_object(v, "x");

                BOOST_REQUIRE(o_v);
                BOOST_TEST(o_v->contains("y"));
            }

            BOOST_AUTO_TEST_CASE(wrong_type_)
            {
                json::array v { 1, 2 };

                auto o_v = json::find_object(v, "x");

                BOOST_REQUIRE(!o_v);
            }

            BOOST_AUTO_TEST_CASE(wrong_key_)
            {
                json::object v { { "y", { { "z", true } } } };

                auto o_v = json::find_object(v, "x");

                BOOST_REQUIRE(!o_v);
            }

            BOOST_AUTO_TEST_CASE(wrong_value_)
            {
                json::object v { { "x", true } };

                auto o_v = json::find_object(v, "x");

                BOOST_REQUIRE(!o_v);
            }

        BOOST_AUTO_TEST_SUITE_END() // object_

        BOOST_AUTO_TEST_CASE(array_)
        {
            json::array v { 1, { { "bar", true } } };

            auto o_v = json::find_object(v, 1);

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->contains("bar"));
        }

        BOOST_AUTO_TEST_CASE(array_const_)
        {
            json::array const v { 1, { { "bar", true } } };

            auto o_v = json::find_object(v, 1);

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->contains("bar"));
        }

    BOOST_AUTO_TEST_SUITE_END() // level_1_

    BOOST_AUTO_TEST_SUITE(level2_)

        BOOST_AUTO_TEST_CASE(object_object_)
        {
            json::object v { { "x", { { "y", { { "z", true } } } } } };

            auto o_v = json::find_object(v, "x", "y");

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->contains("z"));
        }

        BOOST_AUTO_TEST_CASE(object_array_)
        {
            json::object v { { "x", { 1, { { "z", true } } } } };

            auto o_v = json::find_object(v, "x", 1);

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->contains("z"));
        }

        BOOST_AUTO_TEST_CASE(array_object_)
        {
            json::array v { 0, { { "x", { { "z", true } } } } };

            auto o_v = json::find_object(v, 1, "x");

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->contains("z"));
        }

        BOOST_AUTO_TEST_CASE(array_array_)
        {
            json::array v { 0, { 1, { { "z", true } } } };

            auto o_v = json::find_object(v, 1, 1);

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->contains("z"));
        }

    BOOST_AUTO_TEST_SUITE_END() // level2_

BOOST_AUTO_TEST_SUITE_END() // find_object_

BOOST_AUTO_TEST_SUITE(find_array_)

    BOOST_AUTO_TEST_SUITE(level_0_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            json::array v { 1, 2, };

            auto o_v = json::find_array(v);

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->at(1) == 2);
        }

        BOOST_AUTO_TEST_CASE(failure_)
        {
            json::value v { "foo" };

            auto o_v = json::find_array(v);

            BOOST_REQUIRE(!o_v);
        }

    BOOST_AUTO_TEST_SUITE_END() // level_0_

    BOOST_AUTO_TEST_SUITE(level_1_)

        BOOST_AUTO_TEST_SUITE(object_)

            BOOST_AUTO_TEST_CASE(success_)
            {
                json::object v { { "x", { 1, 2, } } };

                auto o_v = json::find_array(v, "x");

                BOOST_REQUIRE(o_v);
                BOOST_TEST(o_v->size() == 2);
            }

            BOOST_AUTO_TEST_CASE(wrong_type_)
            {
                json::value v { "x" };

                auto o_v = json::find_array(v, "x");

                BOOST_REQUIRE(!o_v);
            }

            BOOST_AUTO_TEST_CASE(wrong_key_)
            {
                json::object v { { "y", { 1, 2 } } };

                auto o_v = json::find_array(v, "x");

                BOOST_REQUIRE(!o_v);
            }

            BOOST_AUTO_TEST_CASE(wrong_value_)
            {
                json::object v { { "x", true } };

                auto o_v = json::find_array(v, "x");

                BOOST_REQUIRE(!o_v);
            }

        BOOST_AUTO_TEST_SUITE_END() // object_

        BOOST_AUTO_TEST_CASE(array_)
        {
            json::array v { 1, { 2, 3 } };

            auto o_v = json::find_array(v, 1);

            BOOST_REQUIRE(o_v);
            BOOST_TEST(o_v->at(1) == 3);
        }

    BOOST_AUTO_TEST_SUITE_END() // level_1_

BOOST_AUTO_TEST_SUITE_END() // find_array_

} // namespace testing
