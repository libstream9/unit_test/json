#include <stream9/json/value_from.hpp>

#include "namespace.hpp"

#include <stream9/json/find.hpp>

#include <filesystem>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace fs { using namespace std::filesystem; }

BOOST_AUTO_TEST_SUITE(value_from_)

    BOOST_AUTO_TEST_CASE(path_)
    {
        fs::path p { "/home" };

        auto v = json::value_from(p);

        BOOST_TEST(json::find_string(v) == "/home");
    }

BOOST_AUTO_TEST_SUITE_END() // value_from_

} // namespace testing
